import { apiConfig } from './api.config'

export const environment = {
  production: false,
  apiRoot: "https://apis.sieududoan.com",
  api: apiConfig
};

environment.api.fnSetup(environment.apiRoot);