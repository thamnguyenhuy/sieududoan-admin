
export const apiConfig = {
  init_endpoint_url(object, fields, rootUrl) {
    if (object && fields) {
      fields.forEach(element => {
        object[element] = rootUrl + object[element];
      });
    }
  },
  urls: {
    user: {
      login: "/login",
      logout: "/logout"
    },
    contests: {
      list: "/contests",
      add: "/contests",
      edit: "/contests/{0}",
      delete: "/contests/{0}",
      get: "/contests/{0}",
      order: "/contests/{0}/{1}",
      clone: "/contests/{0}",
      // close_and_calculate_point:"/contests/{0}/update_status/update_point",
    },
    contest_questions: {
      list: "/contest-questions/contest/{0}",
      add: "/contest-questions",
      edit: "/contest-questions/{0}",
      delete: "/contest-questions/{0}",
      get: "/contest-questions/{0}",
      order: "/contest-questions/{0}/order",
      clone: "/contest-questions/{0}",
      update_correct_answer: "/contest-questions/{0}/correct-answer/update",
    },
    contest_winners: {
      list: "/contest-winners/{0}",
      add: "/contest-winners",
      edit: "/contest-winners/{0}",
      delete: "/contest-winners/{0}",
      get: "/contest-winners/{0}",
      order: "/contest-winners/{0}/order",
      up_ward: "/contest-winners/{0}/up_ward",
      down_ward: "/contest-winners/{0}/down_ward",
    },
    helps: {
      list: "/helps",
      add: "/helps",
      edit: "/helps/{0}",
      delete: "/helps/{0}",
      get: "/helps/{0}",
      order: "/helps/{0}/{1}",
      clone: "/helps/{0}/clone",
    },
    user_contests: {
      list: "/user-contests/{0}",
      update_user_contest_point: "/user-contests/{0}",
    },
    metadatas: {
      list: "/metadatas",
      add: "/metadatas",
      edit: "/metadatas/{0}",
      delete: "/metadatas/{0}",
      get: "/metadatas/{0}",
      get_by_url: "/metadatas/url/{0}",
      order: "/contests/{0}/{1}",
    },
    writers: {
      write: "/file-writers"
    }
  },
  fnSetup: function (apiRoot) {
    this.init_endpoint_url(apiConfig.urls.user, Object.getOwnPropertyNames(apiConfig.urls.user), apiRoot);
    this.init_endpoint_url(apiConfig.urls.contests, Object.getOwnPropertyNames(apiConfig.urls.contests), apiRoot);
    this.init_endpoint_url(apiConfig.urls.contest_questions, Object.getOwnPropertyNames(apiConfig.urls.contest_questions), apiRoot);
    this.init_endpoint_url(apiConfig.urls.helps, Object.getOwnPropertyNames(apiConfig.urls.helps), apiRoot);
    this.init_endpoint_url(apiConfig.urls.contest_winners, Object.getOwnPropertyNames(apiConfig.urls.contest_winners), apiRoot);
    this.init_endpoint_url(apiConfig.urls.user_contests, Object.getOwnPropertyNames(apiConfig.urls.user_contests), apiRoot);
    this.init_endpoint_url(apiConfig.urls.metadatas, Object.getOwnPropertyNames(apiConfig.urls.metadatas), apiRoot);
  }
};
