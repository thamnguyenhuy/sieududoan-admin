// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { apiConfig } from './api.config'

export const environment = {
  production: false,
  apiRoot: "http://localhost:3040",
  api: apiConfig
};

environment.api.fnSetup(environment.apiRoot);
console.log(environment);
