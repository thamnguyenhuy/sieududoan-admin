import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContestComponent, ContestDetailComponent } from './contest/contest.index';
import { ContestQuestionComponent, ContestQuestionDetailComponent } from './contestquestion/contestquestion.index';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { QuestionAnswerComponent, QuestionAnswerDetailComponent } from './questionanswer/questionanswer.index';
import { MetaDataComponent, MetaDataDetailComponent } from './metadata/metadata.index';
import { ContestResultDetailComponent } from './contest/contest-result-detail/contest-result-detail.component';

const routes: Routes = [
  { path: 'contests', component: ContestComponent },
  { path: 'contests/:id', component: ContestDetailComponent },
  { path: 'contests/:id/results', component: ContestResultDetailComponent },
  { path: 'contests/:contest_id/questions', component: ContestQuestionComponent },
  { path: 'contests/:contest_id/questions/:question_id', component: ContestQuestionDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'questions-answers', component: QuestionAnswerComponent },
  { path: 'questions-answers/:id', component: QuestionAnswerDetailComponent },
  { path: 'metadatas', component: MetaDataComponent },
  { path: 'metadatas/:id', component: MetaDataDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
