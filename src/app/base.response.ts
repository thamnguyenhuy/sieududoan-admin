export class BaseResponseSingle {
    meta: BaseResponseMeta;
    errors: BaseError[];
    item: any;
}

export class BaseResponseCollection {
    meta: BaseResponseMeta;
    errors: BaseError[];
    items: any[];
}

export class BaseResponseMeta {
    next:string;
    current:string;
    previous:string;
    count:number;
}

export class BaseError {
    code:string;
    message:string;
}


export interface IUserProfile {
    full_name: string,
    social_user_id: string,
    social_provider: string,
    is_logged_in: boolean,
    is_locked: boolean,
    email_address: string,
    photo_url: string,
    social_access_token: string,
};

export interface IUserAccessToken {
    full_name: string,
    access_token: string,
    photo_url: string,
    success: boolean
};