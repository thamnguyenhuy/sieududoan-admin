import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from '../../environments/environment';
import { IQuestionAnswer} from './questionanswer.model';
declare var require: any
var format = require('string-format')

@Injectable()
export class QuestionAnswerService extends BaseService {

  constructor(protected http:HttpClient) { 
      super(http);
  }

  public list() {
    return this.__list(environment.api.urls.helps.list);
  }

  public get(id:string) {
    return this.__get(format(environment.api.urls.helps.get, id));
  }

  public save(data:IQuestionAnswer) {
    if (data._id){
      return this.__edit(format(environment.api.urls.helps.edit, data._id), data);
    } else {
      return this.__add(environment.api.urls.helps.add, data);
    }
  }

  public delete(id:string) {
      return this.__delete(format(environment.api.urls.helps.delete, id));
  }

  public re_order_items(id:string, new_position: number) {
      return this.__re_order_items(format(environment.api.urls.helps.order, id, new_position));
  }

public copy_one_item(id:string) {
    return this.__copy_one_item(format(environment.api.urls.helps.clone, id));
  }
}