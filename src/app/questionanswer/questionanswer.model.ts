
export interface IQuestionAnswer {
  _id: string,
  question_text: string,
  question_asked_by: string,
  question_asked_when: string,
  answer: string,
  position: number,
}
export class QuestionAnswerUtils {
  public static init_default_value(): IQuestionAnswer {
    return {
      _id: "", question_text: '',
      question_asked_by: '',
      question_asked_when: '',
      answer: '',
      position: 0,

    }
  }
}

