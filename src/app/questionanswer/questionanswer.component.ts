import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { BaseResponseCollection } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { QuestionAnswerService } from './questionanswer.service';
import { IQuestionAnswer, QuestionAnswerUtils } from './questionanswer.model'

@Component({
  selector: 'app-questionanswer',
  templateUrl: './questionanswer.component.html',
  styleUrls: ['./questionanswer.component.scss']
})

export class QuestionAnswerComponent extends BaseComponent implements OnInit {
  list_questionanswer: IQuestionAnswer[];
  selected_questionanswer:IQuestionAnswer;
  displayed_columns: string[] = [ 'question_text',  'question_asked_by',  'question_asked_when',  'position',  'action' ]; 
  constructor(private service: QuestionAnswerService, private router:Router) { 
    super();
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.on_load();
  }

  on_load() {
    this.service.list()
      .subscribe(
        (response:BaseResponseCollection) => {
          this.list_questionanswer = response.items;
        }
      );
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
       active: true,
      text: 'Questions And Answers',
      url: '/questions-answers',
      icon: ''
    });
  }

  on_row_delete(questionanswer:IQuestionAnswer){
    this.service.delete(questionanswer._id)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load();
        }
      );
  }

  add_click() {
    this.router.navigateByUrl(`/questions-answers/new`);
  }

  on_drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list_questionanswer, event.previousIndex, event.currentIndex); 
    const move_item = this.list_questionanswer[event.currentIndex];
    this.service.re_order_items(move_item._id, event.currentIndex)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load();
        }
      );
  }
}