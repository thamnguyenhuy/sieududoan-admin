import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuestionAnswerService } from '../questionanswer.service';

@Injectable()
export class QuestionAnswerDetailService extends QuestionAnswerService {

  constructor(protected http:HttpClient) { 
      super(http);
  }
}