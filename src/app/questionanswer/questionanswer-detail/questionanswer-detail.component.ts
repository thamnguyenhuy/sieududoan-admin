import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IQuestionAnswer, QuestionAnswerUtils } from '../questionanswer.model';
import { QuestionAnswerDetailService } from './questionanswer-detail.service';

@Component({
  selector: 'app-questionanswer-detail',
  templateUrl: './questionanswer-detail.component.html',
  styleUrls: ['./questionanswer-detail.component.scss']
})
export class QuestionAnswerDetailComponent extends BaseComponent implements OnInit {
  item: IQuestionAnswer;
  constructor(private service: QuestionAnswerDetailService, private route: ActivatedRoute, private router: Router) {
    super();
    this.item = QuestionAnswerUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

 ngOnInit() {
    this.init_top_nav_items();
    this.route.params.subscribe(res => {
      this.item = QuestionAnswerUtils.init_default_value();
      if (res.id) {
        this.service.get(res.id).subscribe(
          (response: BaseResponseSingle) => {
            this.item = response.item;
            this.init_top_nav_items();
          }
        );
      }
    });
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'Questions And Answers',
      url: '/questions-answers',
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Edit Questions And Answers' : 'New Questions And Answers', 
      url: "/questions-answers/" + this.item._id,
      icon: ''
    });
  }

  on_save_click() {
    this.service.save(this.item).subscribe(
      (response: BaseResponseSingle) => {
        this.router.navigateByUrl("/questions-answers");
      }
    );
  }

  on_close_click() {
        this.router.navigateByUrl("/questions-answers");
  }

  on_copy_click() {
    if (this.item._id) {
      this.item._id = "";
      this.item.question_text = "Copy " + this.item.question_text;
      this.service.save(this.item).subscribe(
        (response: BaseResponseSingle) => {          
          this.router.navigateByUrl("/questions-answers");
        }
      );
    }
  }
}