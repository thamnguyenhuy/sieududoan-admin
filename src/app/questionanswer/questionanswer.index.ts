export * from './questionanswer.component';
export * from './questionanswer.service';
export * from './questionanswer.model';

export * from './questionanswer-detail/questionanswer-detail.component';
export * from './questionanswer-detail/questionanswer-detail.service';
