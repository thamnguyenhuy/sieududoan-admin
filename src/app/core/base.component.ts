import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ITopNav } from '../shared/top-nav/top-nav.model';
import { ITopNavConfig } from '../shared/top-nav/top-nav.component';

declare var require: any;
const uuidv1 = require('uuid/v1');

export class BaseComponent {
    @Output() close: EventEmitter<any> = new EventEmitter();
    
    uuid: string = uuidv1();
    primary_key = '';
    random: string;
    top_nav_items: ITopNav[] = [];
    grid_view: boolean = true;
    top_nav_config: ITopNavConfig;

    constructor() {
        this.top_nav_config = new ITopNavConfig();
    }

    public get_random_string(): string {
        return uuidv1();
    }

    public refresh_random_string() {
        this.random = uuidv1();
    }

    view_mode_click(grid_view: boolean) {
        this.grid_view = grid_view;
    }

    init_toolbar_buttons(show_add_button:boolean, show_view_mode_button: boolean, show_refresh_button: boolean=false) {
        this.top_nav_config.show_add_button = show_add_button;
        this.top_nav_config.show_view_mode_buttons = show_view_mode_button;
        this.top_nav_config.show_refresh_button = show_refresh_button;
    }
}
