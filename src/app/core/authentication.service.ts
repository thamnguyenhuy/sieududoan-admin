import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { IUserAccessToken, IUserProfile } from '../base.response';
import { FacebookService, LoginOptions, LoginResponse, InitParams } from 'ngx-facebook';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
    @Output() get_login_user: EventEmitter<any> = new EventEmitter();
    constructor(protected http: HttpClient, private fb: FacebookService, private router: Router) {
        let initParams: InitParams = {
            appId: '302041533670626',
            xfbml: true,
            version: 'v2.8'
        };

        this.fb.init(initParams);
    }

    private api_login(data: IUserProfile) {
        return this.http.post(environment.api.urls.user.login, data);
    }

    public log_out() {
        localStorage.setItem('auth', null);
        this.get_login_user.emit(null);
    }

    public save_login(auth: IUserAccessToken) {
        localStorage.setItem('auth', JSON.stringify(auth));
    }

    public get_saved_auth(): IUserAccessToken {
        let auth: IUserAccessToken = JSON.parse(localStorage.getItem('auth'));
        if (!auth) {
            auth = {
                full_name: "",
                photo_url: "",
                access_token: "",
                success: false
            }
        }
        return auth;
    }

    public is_logged_in(): boolean {
        const auth = this.get_saved_auth();
        return auth != null && auth.success;
    }

    public fb_login(successful_nav_url: string, fail_nav_url: string) {
        const options: LoginOptions = {
            scope: 'public_profile,email',
            return_scopes: true,
            enable_profile_selector: true
        };
        this.fb.login(options)
            .then((res: LoginResponse) => {
                if (res && res.status === 'connected') {
                    let profile: IUserProfile = {
                        social_access_token: res.authResponse.accessToken,
                        full_name: "",
                        social_user_id: res.authResponse.userID,
                        social_provider: "facebook",
                        is_logged_in: true,
                        is_locked: false,
                        email_address: "",
                        photo_url: "",
                    }
                    this.api_login(profile).subscribe((response: IUserAccessToken) => {
                        if (response && response.success) {
                            this.save_login(response);
                            this.get_login_user.emit(response);
                            if (successful_nav_url) {
                                this.router.navigate([successful_nav_url]);
                            }
                        }
                    });
                }
            })
            .catch(this.handle_fb_login_error);
    }

    private handle_fb_login_error(error) {
        console.error('Error processing action', error);
    }
}