import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class BaseService {

  constructor(protected http: HttpClient) { }

  public __list(url) {
    return this.http.get(url);
  }

  public __get(url) {
    return this.http.get(url);
  }

  public __add(url, data) {
    return this.http.post(url, data);
  }

  public __edit(url, data) {
    return this.http.put(url, data);
  }

  public __delete(url) {
    return this.http.delete(url);
  }

  public __write(content: string, filepath: string) {
    var data = {
      content: content,
      filepath: filepath
    }
    return this.http.post(environment.api.urls.writers.write, data);
  }

  public __re_order_items(url) {
    return this.http.post(url, {});
  }

  public __copy_one_item(url) {
    return this.http.post(url, {});
  }
}
