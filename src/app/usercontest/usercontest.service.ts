import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from '../../environments/environment';
import { IUserContest} from './usercontest.model';
declare var require: any
var format = require('string-format')

@Injectable()
export class UserContestService extends BaseService {

  constructor(protected http:HttpClient) { 
      super(http);
  }

  public list(contest_id: string) {
    return this.__list(format(environment.api.urls.user_contests.list, contest_id));
  }

//   public get(id:string) {
//     return this.__get(format(environment.api.urls.user-contest.get, id));
//   }

//   public save(data:IUserContest) {
//     if (data._id){
//       return this.__edit(format(environment.api.urls.user-contest.edit, data._id), data);
//     } else {
//       return this.__add(environment.api.urls.user-contest.add, data);
//     }
//   }

//   public delete(id:string) {
//       return this.__delete(format(environment.api.urls.user-contest.delete, id));
//   }

//   public re_order_items(id:string, new_position: number) {
//       return this.__re_order_items(format(environment.api.urls.user-contest.order, id, new_position));
//   }

// public copy_one_item(id:string) {
//     return this.__copy_one_item(format(environment.api.urls.user-contest.clone, id));
//   }
}