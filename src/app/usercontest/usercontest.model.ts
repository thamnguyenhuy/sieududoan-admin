export interface IUserContest {
  _id: string,
  user_id: string,
  contest_id: string,
  total_point: number,
  point_updated_at: number,
  ranking: number,
  full_name: string;
  social_user_id: string,
  social_provider: string,
  photo_url: string,
}
export class UserContestUtils {
  public static init_default_value(): IUserContest {
    return {
      _id: "", user_id: '',
      contest_id: '',
      total_point: 0,
      point_updated_at: 0,
      ranking: 0,
      full_name: '',
      social_user_id: '',
      social_provider: '',
      photo_url: ''
    }
  }
}

