import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserContestService } from '../usercontest.service';

@Injectable()
export class UserContestDetailService extends UserContestService {

  constructor(protected http:HttpClient) { 
      super(http);
  }
}