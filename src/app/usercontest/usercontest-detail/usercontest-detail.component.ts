import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IUserContest, UserContestUtils } from '../usercontest.model';
import { UserContestDetailService } from './usercontest-detail.service';

@Component({
  selector: 'app-usercontest-detail',
  templateUrl: './usercontest-detail.component.html',
  styleUrls: ['./usercontest-detail.component.scss']
})
export class UserContestDetailComponent extends BaseComponent implements OnInit {
  item: IUserContest;
  constructor(private service: UserContestDetailService, private route: ActivatedRoute, private router: Router) {
    super();
    this.item = UserContestUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

 ngOnInit() {
    this.init_top_nav_items();
    this.route.params.subscribe(res => {
      this.item = UserContestUtils.init_default_value();
      if (res.id) {
        // this.service.list(res.id).subscribe(
        //   (response: BaseResponseSingle) => {
        //     this.item = response.item;
        //     this.init_top_nav_items();
        //   }
        // );
      }
    });
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'User Contests',
      url: '/user-contest',
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Edit User Contests' : 'New User Contests', 
      url: "/user-contest/" + this.item._id,
      icon: ''
    });
  }

  on_save_click() {
    // this.service.save(this.item).subscribe(
    //   (response: BaseResponseSingle) => {
    //     this.router.navigateByUrl("/user-contest");
    //   }
    // );
  }

  on_close_click() {
        this.router.navigateByUrl("/user-contest");
  }
}