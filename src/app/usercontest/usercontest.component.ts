import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { BaseResponseCollection } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { UserContestService } from './usercontest.service';
import { IUserContest, UserContestUtils } from './usercontest.model'

@Component({
  selector: 'app-usercontest',
  templateUrl: './usercontest.component.html',
  styleUrls: ['./usercontest.component.scss']
})

export class UserContestComponent extends BaseComponent implements OnInit {
  @Input("contest_id") contest_id: string;
  @Output("add_winner") add_winner: EventEmitter<IUserContest> = new EventEmitter();
  list_usercontest: IUserContest[];
  selected_usercontest:IUserContest;
  displayed_columns: string[] = [ 'user_id',  'full_name', 'social_provider' ,  'total_point',  'point_updated_at',  'ranking', 'action']; 
  constructor(private service: UserContestService, private router:Router) { 
    super();
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.init_toolbar_buttons(false, false);
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(this.contest_id);
    if (this.contest_id) {
      this.on_load(this.contest_id);
    }
  }

  on_load(contest_id: string) {
    this.service.list(contest_id)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.list_usercontest = response.items;
        }
      );
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
       active: true,
      text: 'User Contests',
      url: '',
      icon: ''
    });
  }

  on_row_add_winner(user_contest:IUserContest){
    this.add_winner.emit(user_contest);
  }

  add_click() {
    this.router.navigateByUrl(`/user-contest/new`);
  }

  on_drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list_usercontest, event.previousIndex, event.currentIndex); 
    const move_item = this.list_usercontest[event.currentIndex];
    // this.service.re_order_items(move_item._id, event.currentIndex)
    //   .subscribe(
    //     (response:BaseResponseCollection) => {
    //       this.on_load();
    //     }
    //   );
  }
}