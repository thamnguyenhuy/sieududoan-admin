import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContestQuestionService } from '../contestquestion.service';

@Injectable()
export class ContestQuestionDetailService extends ContestQuestionService {

  constructor(protected http:HttpClient) { 
      super(http);
  }
}