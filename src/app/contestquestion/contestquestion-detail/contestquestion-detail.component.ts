import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IContestQuestion, ContestQuestionUtils } from '../contestquestion.model';
import { ContestQuestionDetailService } from './contestquestion-detail.service';

@Component({
  selector: 'app-contestquestion-detail',
  templateUrl: './contestquestion-detail.component.html',
  styleUrls: ['./contestquestion-detail.component.scss']
})
export class ContestQuestionDetailComponent extends BaseComponent implements OnInit {
  item: IContestQuestion;
  question_items:string;
  contest_id:string;
  correct_answer: string;

  constructor(private service: ContestQuestionDetailService, private route: ActivatedRoute, private router: Router) {
    super();
    this.item = ContestQuestionUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

 ngOnInit() {
    this.item = ContestQuestionUtils.init_default_value();

    this.route.params.subscribe(res => {
      this.init_top_nav_items(res.contest_id, "");
      if (res.question_id) {
        this.contest_id = res.contest_id;
        this.service.get(res.question_id).subscribe(
          (response: BaseResponseSingle) => {
            this.item = response.item;
            this.question_items = this.item.question_items.join('\r\n');
            this.init_top_nav_items(this.item.contest_id, this.item._id);
          }
        );
      }
    });
  }

  init_top_nav_items(contest_id:string, question_id:string) {
    this.top_nav_items = [];
    this.top_nav_items.push({
       active: true,
      text: 'Contests',
      url: '/contests',
      icon: ''
    }, {
       active: true,
      text: 'Questions',
      url: `/contests/${contest_id}/questions`,
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Edit Questions' : 'New Questions', 
      url: `/contests/${contest_id}/questions/${this.item._id}`,
      icon: ''
    });
  }

  on_save_click() {
    this.item.question_items = this.question_items.match(/[^\r\n]+/g);
    this.item.contest_id = this.contest_id;
    this.service.save(this.item).subscribe(
      (response: BaseResponseSingle) => {
        this.router.navigateByUrl(`/contests/${this.contest_id}/questions`,);
      }
    );
  }

  on_close_click() {
    this.router.navigateByUrl(`/contests/${this.contest_id}/questions`,);
  }

  on_save_correct_answer_click() {
    this.service.update_correct_anwser(this.item._id, this.item.correct_answer).subscribe(
      (response: BaseResponseSingle) => {
      }
    );
  }

  on_copy_click() {
    if (this.item._id) {
      this.item._id = "";
      this.item.question_text = "Copy " + this.item.question_text;
      this.service.save(this.item).subscribe(
        (response: BaseResponseSingle) => {          
          this.router.navigateByUrl(`/contests/${this.contest_id}/questions`,);
        }
      );
    }
  }
}