import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from '../../environments/environment';
import { IContestQuestion } from './contestquestion.index';
declare var require: any
var format = require('string-format')

@Injectable()
export class ContestQuestionService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  public list(contest_id: string) {
    return this.__list(format(environment.api.urls.contest_questions.list, contest_id));
  }

  public get(question_id: string) {
    return this.__get(format(environment.api.urls.contest_questions.get, question_id));
  }

  public save(data: IContestQuestion) {
    if (data._id) {
      return this.__edit(format(environment.api.urls.contest_questions.edit, data._id), data);
    } else {
      return this.__add(environment.api.urls.contest_questions.add, data);
    }
  }

  public delete(id: string) {
    return this.__delete(format(environment.api.urls.contest_questions.delete, id));
  }

  public re_order_items(id: string, new_position: number) {
    return this.__re_order_items(format(environment.api.urls.contest_questions.order, id, new_position));
  }

  public copy_one_item(id: string) {
    return this.__copy_one_item(format(environment.api.urls.contest_questions.clone, id));
  }

  public update_correct_anwser(id:string, correct_answer: string) {
    const data = {
      "correct_answer" : correct_answer
    }
    return this.__add(format(environment.api.urls.contest_questions.update_correct_answer, id), data);
  }
}