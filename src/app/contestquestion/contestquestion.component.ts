import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { BaseResponseCollection } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { ContestQuestionService } from './contestquestion.service';
import { IContestQuestion, ContestQuestionUtils } from './contestquestion.model'

@Component({
  selector: 'app-contestquestion',
  templateUrl: './contestquestion.component.html',
  styleUrls: ['./contestquestion.component.scss']
})

export class ContestQuestionComponent extends BaseComponent implements OnInit {
  list_contestquestion: IContestQuestion[];
  selected_contestquestion:IContestQuestion;
  displayed_columns: string[] = [ 'question_text', 'close_time',  'bonus_point',  'correct_answer',  'action' ]; 
  contest_id: string;

  constructor(private service: ContestQuestionService, private route: ActivatedRoute, private router: Router) { 
    super();
  }

  ngOnInit() {
    this.route.params.subscribe(res => {
      if (res.contest_id) {
        this.contest_id = res.contest_id;
        this.init_top_nav_items(this.contest_id);
        this.on_load(this.contest_id);
      }
    });
    
  }

  on_load(contest_id: string) {
    this.service.list(contest_id)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.list_contestquestion = response.items;
        }
      );
  }

  init_top_nav_items(contest_id:string) {
    this.top_nav_items = [];
    this.top_nav_items.push({
       active: true,
      text: 'Contests',
      url: '/contests',
      icon: ''
    }, {
       active: true,
      text: 'Questions',
      url: `/contest/${contest_id}`,
      icon: ''
    });
  }

  on_row_delete(contestquestion:IContestQuestion){
    this.service.delete(contestquestion._id)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load(this.contest_id);
        }
      );
  }

  add_click() {
    this.router.navigateByUrl(`/contests/${this.contest_id}/questions/new`);
  }

  on_drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list_contestquestion, event.previousIndex, event.currentIndex); 
    const move_item = this.list_contestquestion[event.currentIndex];
    this.service.re_order_items(move_item._id, event.currentIndex)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load(this.contest_id);
        }
      );
  }
}