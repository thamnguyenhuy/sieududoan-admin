








export interface IContestQuestion {
  _id: string,
  question_text: string,
  contest_id: string,
  question_items: string[],
  question_external_url: string,
  close_time: number,
  bonus_point: number,
  correct_answer: string,
  position: number,

}
export class ContestQuestionUtils {
  public static init_default_value(): IContestQuestion {
    return {
      _id: "", question_text: '',
      contest_id: '',
      question_items: [],
      question_external_url: '',
      close_time: 0,
      bonus_point: 0,
      correct_answer: '',
      position: 0,

    }
  }
}

