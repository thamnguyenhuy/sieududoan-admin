export interface ITopNav {
    url: string;
    text: string;
    icon: string;
    active: boolean;
}
