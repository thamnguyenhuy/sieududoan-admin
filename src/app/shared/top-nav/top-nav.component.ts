import { Component, OnInit, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { ITopNav } from './top-nav.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {
  @Input('items') items: ITopNav[];
  @Input('config') config: ITopNavConfig;
  @Output('view_mode_click') view_mode_click: EventEmitter<boolean> = new EventEmitter();
  @Output('add_click') add_click: EventEmitter<any> = new EventEmitter();
  @Output('refresh_click') refresh_click: EventEmitter<any> = new EventEmitter();

  grid_view: boolean = true;
  constructor(private router:Router) { 
    this.config = new ITopNavConfig();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  left_item_on_click(item:ITopNav) {
    if (item.url) {
      this.router.navigate([item.url]);
    }
  }

  on_view_mode_click() {
    this.grid_view = !this.grid_view;
    this.view_mode_click.emit(this.grid_view);
  }

  on_add_click() {
    this.add_click.emit();
  }

  on_refresh_click() {
    this.refresh_click.emit();
  }
}

export class ITopNavConfig {
  show_view_mode_buttons: boolean = true;
  show_add_button: boolean = true;
  show_refresh_button: boolean = false;
}