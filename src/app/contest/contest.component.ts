import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { BaseResponseCollection, BaseResponseSingle } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { ContestService } from './contest.service';
import { IContest, ContestUtils } from './contest.model'
import { IMetaData, MetaDataUtils, MetaDataDetailService } from '../metadata/metadata.index';

@Component({
  selector: 'app-contest',
  templateUrl: './contest.component.html',
  styleUrls: ['./contest.component.scss']
})

export class ContestComponent extends BaseComponent implements OnInit {
  list_contest: IContest[];
  selected_contest:IContest;
  displayed_columns: string[] = [ 'contest_url', 'questions', 'result', 'contest_name', 'start_time', 'end_time', 'contest_status', "metadata"]; 
  constructor(private service: ContestService, private router:Router, private meta_service: MetaDataDetailService) { 
    super();
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.on_load();
  }

  on_load() {
    this.service.list()
      .subscribe(
        (response:BaseResponseCollection) => {
          this.list_contest = response.items;
        }
      );
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
       active: true,
      text: 'Contests',
      url: '/contests',
      icon: ''
    });
  }

  on_row_delete(contest:IContest){
    this.service.delete(contest._id)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load();
        }
      );
  }

  add_click() {
    this.router.navigateByUrl(`/contests/new`);
  }

  on_drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list_contest, event.previousIndex, event.currentIndex); 
    const move_item = this.list_contest[event.currentIndex];
    this.service.re_order_items(move_item._id, event.currentIndex)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load();
        }
      );
  }

  on_metadata_click(contest:IContest) {
    let item : IMetaData = {
      _id: "",
      language: "vi",
      article_title: contest.contest_name,
      description: contest.description_short,
      keywords: contest.contest_name,
      article_url: "contests/" + contest.contest_url,
      image_url: contest.thumbnail_url,
      article_type: 'Article',
      article_detail: contest.description_full,
    }
    const article_url = "contests%2F" + contest.contest_url;
    this.meta_service.get_by_url(article_url).subscribe((response: BaseResponseSingle) => {
      const saved_item = response.item;
      if (!saved_item._id) {
        this.meta_service.save(item).subscribe(
          (response: BaseResponseSingle) => {
            this.router.navigateByUrl("/metadatas/" + response.item._id);
          }
        );
      }
    });
  }
}