import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from '../../environments/environment';
import { IContest } from './contest.model';
declare var require: any
var format = require('string-format')

@Injectable()
export class ContestService extends BaseService {

  constructor(protected http:HttpClient) { 
      super(http);
  }

  public list() {
    return this.__list(environment.api.urls.contests.list);
  }

  public get(id:string) {
    return this.__get(format(environment.api.urls.contests.get, id));
  }

  public save(data:IContest) {
    if (data._id){
      return this.__edit(format(environment.api.urls.contests.edit, data._id), data);
    } else {
      return this.__add(environment.api.urls.contests.add, data);
    }
  }

  public delete(id:string) {
      return this.__delete(format(environment.api.urls.contests.delete, id));
  }

  public re_order_items(id:string, new_position: number) {
      return this.__re_order_items(format(environment.api.urls.contests.order, id, new_position));
  }

  public copy_one_item(id:string) {
    return this.__copy_one_item(format(environment.api.urls.contests.clone, id));
  }

  public close_contest_and_calculate_player_points(id: string, status: string) {    
  }

  public update_user_contest_point(contest_id: string) {
      return this.__add(format(environment.api.urls.user_contests.update_user_contest_point, contest_id), null);
  }
}