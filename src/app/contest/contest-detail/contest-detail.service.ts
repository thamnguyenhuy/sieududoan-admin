import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContestService } from '../contest.service';

@Injectable()
export class ContestDetailService extends ContestService {

  constructor(protected http:HttpClient) { 
      super(http);
  }
}