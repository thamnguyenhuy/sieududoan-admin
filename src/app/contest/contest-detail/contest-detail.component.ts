import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle, BaseResponseCollection } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IContest, ContestUtils } from '../contest.model';
import { ContestDetailService } from './contest-detail.service';
import { ContestQuestionService, ContestQuestionDetailService, ContestQuestionUtils, IContestQuestion } from 'src/app/contestquestion/contestquestion.index';
import * as marked from 'marked';

@Component({
  selector: 'app-contest-detail',
  templateUrl: './contest-detail.component.html',
  styleUrls: ['./contest-detail.component.scss']
})
export class ContestDetailComponent extends BaseComponent implements OnInit {
  item: IContest;
  contest_status_list: string[] = ["Draft", "UpComing", "Active", "Closed", "Deleted"];
  constructor(private service: ContestDetailService, private contest_question_sv: ContestQuestionService, 
    private contest_question_detail_sv: ContestQuestionDetailService,
    private route: ActivatedRoute, private router: Router) {
    super();
    this.item = ContestUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.route.params.subscribe(res => {
      this.item = ContestUtils.init_default_value();
      if (res.id) {
        this.service.get(res.id).subscribe(
          (response: BaseResponseSingle) => {
            this.item = response.item;
            this.init_top_nav_items();
          }
        );
      }
    });
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'Contests',
      url: '/contests',
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Edit Contests' : 'New Contests',
      url: "/contests/" + this.item._id,
      icon: ''
    });
  }

  description_full_changed(event: any) {
    this.item.description_full_marked = marked.parse(this.item.description_full);
  }

  on_save_click() {
    this.service.save(this.item).subscribe(
      (response: BaseResponseSingle) => {
        //update contest result.
        this.service.update_user_contest_point(this.item._id).subscribe((update_response:BaseResponseSingle) => {
          this.router.navigateByUrl("/contests");
        });
      }
    );
  }

  on_close_click() {
    this.router.navigateByUrl("/contests");
  }

  on_copy_click() {
    if (this.item._id) {
      const old_contest_id = this.item._id;
      this.item._id = "";
      this.item.contest_name = "Copy " + this.item.contest_name;
      this.service.save(this.item).subscribe(
        (response: BaseResponseSingle) => {
          const new_contest: IContest = response.item;
          if (new_contest && new_contest._id) {
            //copy all the question detail.
            this.contest_question_sv.list(old_contest_id).subscribe((question_response: BaseResponseCollection) => {
              let list_question: IContestQuestion[] = question_response.items;
              if (list_question && list_question.length > 0) {
                for (let index = 0; index < list_question.length; index++) {
                  let element = list_question[index];
                  element._id = "";
                  element.contest_id = new_contest._id;
                  this.contest_question_detail_sv.save(element).subscribe((save_response:BaseResponseSingle) => {
                  });
                }
              }
            });
          }
          this.router.navigateByUrl("/contests");
        }
      );
    }
  }
}