
export interface IContest {
  _id: string,
  contest_url: string,
  contest_name: string,
  start_time: number,
  end_time: number,
  banner_url: string,
  thumbnail_url: string,
  team_a_name: string,
  team_a_image_url: string,
  team_b_name: string,
  team_b_image_url: string,
  description_short: string,
  description_full: string,
  description_full_marked: string,
  image_urls: string,
  contest_status: string,
  position: number,

}
export class ContestUtils {
  public static init_default_value(): IContest {
    return {
      _id: "", contest_url: '',
      contest_name: '',
      start_time: 0,
      end_time: 0,
      banner_url: '',
      thumbnail_url: '',
      team_a_name: '',
      team_a_image_url: '',
      team_b_name: '',
      team_b_image_url: '',
      description_short: '',
      description_full: '',
      description_full_marked: '',
      image_urls: '',
      contest_status: '',
      position: 0,

    }
  }
}

