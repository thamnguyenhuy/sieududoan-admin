import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle, BaseResponseCollection } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IContest, ContestUtils } from '../../contest/contest.model';
import { ContestDetailService } from '../../contest/contest-detail/contest-detail.service';
import { IUserContest } from 'src/app/usercontest/usercontest.model';
import { ContestWinnerService, IContestWinner } from 'src/app/contestwinner/contestwinner.index';

@Component({
  selector: 'app-contest-result-detail',
  templateUrl: './contest-result-detail.component.html',
  styleUrls: ['./contest-result-detail.component.scss']
})
export class ContestResultDetailComponent extends BaseComponent implements OnInit {
  contest_id: string = "";
  item: IContest;
  contest_status_list: string[] = ["Draft", "UpComing", "Active", "Closed", "Deleted"];
  constructor(private service: ContestDetailService,
    private contest_winner_sv: ContestWinnerService,
    private route: ActivatedRoute, private router: Router) {
    super();
    this.item = ContestUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.route.params.subscribe(res => {
      this.item = ContestUtils.init_default_value();
      if (res.id) {
        this.service.get(res.id).subscribe(
          (response: BaseResponseSingle) => {
            this.item = response.item;
            this.contest_id = this.item._id;
            this.init_top_nav_items();
          }
        );
      }
    });
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'Contests',
      url: '/contests',
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Update Result' : 'New Contests',
      url: "/contests/" + this.item._id + "/results",
      icon: ''
    });
  }

  on_save_click() {
    this.service.save(this.item).subscribe(
      (response: BaseResponseSingle) => {
        this.contest_id = "";
        this.service.update_user_contest_point(this.item._id).subscribe((response_point: BaseResponseCollection) => {
          this.contest_id = this.item._id;
        });
      });
  }

  on_add_winner_click(user_contest: IUserContest) {
    if (user_contest) {
      const contest_winner: IContestWinner = {
        _id: null,
        contest_id: user_contest.contest_id,
        user_id: user_contest.user_id,
        total_point: user_contest.total_point,
        full_name: user_contest.full_name,
        social_user_id: user_contest.social_user_id,
        photo_url: user_contest.photo_url,
        social_provider: user_contest.social_provider,
        ranking: 0,
        updated_at: 0
      };
      this.contest_winner_sv.save(contest_winner).subscribe((response: BaseResponseSingle) => {
        
      });
    }
  }
}