import { OnInit, Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';
import { AuthenticationService } from '../core/authentication.service';

@Component({
  selector: 'app-login-bottom-sheet',
  templateUrl: './login-bottom-sheet.component.html',
  styleUrls: ['./login-bottom-sheet.component.scss']
})
export class LoginBottomSheetComponent implements OnInit {

  constructor(private bottom_sheet: MatBottomSheetRef<LoginBottomSheetComponent>,
  private service: AuthenticationService) {}

  on_login_click(event: MouseEvent): void {
    this.bottom_sheet.dismiss();
    event.preventDefault();
    this.service.fb_login("", "");
  }

  ngOnInit() {
  }
}
