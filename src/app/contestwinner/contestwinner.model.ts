

export interface IContestWinner {
  _id: string,
  contest_id: String,
  user_id: String,
  total_point: Number,
  full_name: String,
  photo_url: String,
  social_provider: String,
  social_user_id: String,
  ranking: Number,
  updated_at: Number,
}
export class ContestWinnerUtils {
  public static init_default_value(): IContestWinner {
    return {
      _id: "",
      contest_id: '',
      user_id: '',
      total_point: 0,
      full_name: '',
      social_user_id: '',
      photo_url: '',
      social_provider: '',
      ranking: 0,
      updated_at: 0
    }
  }
}

