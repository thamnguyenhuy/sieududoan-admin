import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContestWinnerService } from '../contestwinner.service';

@Injectable()
export class ContestWinnerDetailService extends ContestWinnerService {

  constructor(protected http:HttpClient) { 
      super(http);
  }
}