import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IContestWinner, ContestWinnerUtils } from '../contestwinner.model';
import { ContestWinnerDetailService } from './contestwinner-detail.service';

@Component({
  selector: 'app-contestwinner-detail',
  templateUrl: './contestwinner-detail.component.html',
  styleUrls: ['./contestwinner-detail.component.scss']
})
export class ContestWinnerDetailComponent extends BaseComponent implements OnInit {
  item: IContestWinner;
  constructor(private service: ContestWinnerDetailService, private route: ActivatedRoute, private router: Router) {
    super();
    this.item = ContestWinnerUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

 ngOnInit() {
    this.init_top_nav_items();
    this.route.params.subscribe(res => {
      this.item = ContestWinnerUtils.init_default_value();
      if (res.id) {
        this.service.get(res.id).subscribe(
          (response: BaseResponseSingle) => {
            this.item = response.item;
            this.init_top_nav_items();
          }
        );
      }
    });
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'Contest Winners',
      url: '/contest-winners',
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Edit Contest Winners' : 'New Contest Winners', 
      url: "/contest-winners/" + this.item._id,
      icon: ''
    });
  }

  on_save_click() {
    this.service.save(this.item).subscribe(
      (response: BaseResponseSingle) => {
        this.router.navigateByUrl("/contest-winners");
      }
    );
  }

  on_close_click() {
        this.router.navigateByUrl("/contest-winners");
  }
}