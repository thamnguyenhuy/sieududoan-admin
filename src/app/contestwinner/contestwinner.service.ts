import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from '../../environments/environment';
import { IContestWinner } from './contestwinner.model';
declare var require: any
var format = require('string-format')

@Injectable()
export class ContestWinnerService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  public list(contest_id: string) {
    return this.__list(format(environment.api.urls.contest_winners.list, contest_id));
  }

  public get(id: string) {
    return this.__get(format(environment.api.urls.contest_winners.get, id));
  }

  public save(data: IContestWinner) {
    if (data._id) {
      return this.__edit(format(environment.api.urls.contest_winners.edit, data._id), data);
    } else {
      return this.__add(environment.api.urls.contest_winners.add, data);
    }
  }

  public delete(id: string) {
    return this.__delete(format(environment.api.urls.contest_winners.delete, id));
  }

  public re_order_items(id: string, new_position: number) {
    return this.__re_order_items(format(environment.api.urls.contest_winners.order, id, new_position));
  }


  public up_ward(id: string) {
    return this.__edit(format(environment.api.urls.contest_winners.up_ward, id), {});
  }

  public down_ward(id: string) {
    return this.__edit(format(environment.api.urls.contest_winners.down_ward, id), {});
  }

}