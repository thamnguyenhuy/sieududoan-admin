import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { BaseResponseCollection } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { ContestWinnerService } from './contestwinner.service';
import { IContestWinner, ContestWinnerUtils } from './contestwinner.model'

@Component({
  selector: 'app-contestwinner',
  templateUrl: './contestwinner.component.html',
  styleUrls: ['./contestwinner.component.scss']
})

export class ContestWinnerComponent extends BaseComponent implements OnInit {
  @Input("contest_id") contest_id: string;
  list_contestwinner: IContestWinner[];
  selected_contestwinner: IContestWinner;
  displayed_columns: string[] = ['ranking', 'photo_url', 'full_name', 'social_provider', 'total_point', 'updated_at', 'action'];
  constructor(private service: ContestWinnerService, private router: Router) {
    super();
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.init_toolbar_buttons(false, false, true);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`winner = ${this.contest_id}`);
    if (this.contest_id) {
      this.on_load(this.contest_id);
    }
  }

  on_load(contest_id: string) {
    this.service.list(contest_id)
      .subscribe(
        (response: BaseResponseCollection) => {
          this.list_contestwinner = response.items;
        }
      );
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'Contest Winners',
      url: '',
      icon: ''
    });
  }

  on_row_delete(contestwinner: IContestWinner) {
    this.service.delete(contestwinner._id)
      .subscribe(
        (response: BaseResponseCollection) => {
          this.on_load(this.contest_id);
        }
      );
  }


  on_drop(event: CdkDragDrop<string[]>) {
    console.log(event);
    moveItemInArray(this.list_contestwinner, event.previousIndex, event.currentIndex);
    const move_item = this.list_contestwinner[event.currentIndex];
    this.service.re_order_items(move_item._id, event.currentIndex)
      .subscribe(
        (response: BaseResponseCollection) => {
          this.on_load(this.contest_id);
        }
      );
  }

  on_row_up(contest_winner: IContestWinner) {
    this.service.up_ward(contest_winner._id)
      .subscribe(
        (response: BaseResponseCollection) => {
          this.on_load(this.contest_id);
        }
      );
  }

  on_row_down(contest_winner: IContestWinner) {
    this.service.down_ward(contest_winner._id)
      .subscribe(
        (response: BaseResponseCollection) => {
          this.on_load(this.contest_id);
        }
      );
  }

  on_refresh_click() {
    this.on_load(this.contest_id);
  }
}