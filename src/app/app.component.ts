import { Component, ViewChild } from '@angular/core';
import { AuthenticationService } from './core/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IUserAccessToken } from './base.response';
import { UserContestComponent } from './usercontest/usercontest.index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild("snav") snav;
  mat_opened:boolean=false;
  is_logged_in: boolean;
  current_login_user_name:string;
  constructor(private service: AuthenticationService, private route: ActivatedRoute){
    service.get_login_user.subscribe((user:IUserAccessToken) => {
      this.is_logged_in = this.service.is_logged_in();
      this.current_login_user_name = "";
      if (user) {
        this.current_login_user_name = user.full_name = user.full_name;
      }
    });
  }

  ngOnInit() {
    this.is_logged_in = this.service.is_logged_in();
  }

  close_mat_sidenav() {
    this.mat_opened = false;
  }

  menu_click() {
    this.is_logged_in = this.service.is_logged_in()
    this.snav.toggle()
  }

  favourite_click() {

  }
}
