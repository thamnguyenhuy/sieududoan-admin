import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseResponseSingle, IUserAccessToken } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { AuthenticationService } from '../core/authentication.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent  extends BaseComponent implements OnInit {
  user: IUserAccessToken;
  constructor(private snackBar: MatSnackBar, 
    private service: AuthenticationService, private router: Router,
  ) {
   super();
   this.top_nav_config = {
    show_add_button: false,
    show_view_mode_buttons: false,
    show_refresh_button: false,
  }
 }
 
 ngOnInit() {
   this.init_top_nav_items();
   this.on_load();
 }

 on_load() {
   this.user = this.service.get_saved_auth();
 }

 init_top_nav_items() {
   this.top_nav_items = [];
   this.top_nav_items.push({
     active: true,
     text: 'Đăng xuất hệ thống',
     url: '/logout',
     icon: 'exit_to_app',
   });
 }

 on_logged_out_click() {
  this.service.log_out();
  this.snackBar.open("Bạn đang đăng xuất khỏi sieududoan", "", {
    duration: 2000,
  });
  setTimeout(() => {
    this.router.navigate(["login"]);
  }, 2200);
 }
}
