import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MetaDataService } from '../metadata.service';

@Injectable()
export class MetaDataDetailService extends MetaDataService {

  constructor(protected http:HttpClient) { 
      super(http);
  }
}