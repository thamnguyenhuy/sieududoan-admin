import * as _ from 'lodash';
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResponseSingle } from '../../base.response';
import { BaseComponent } from '../../core/base.component';
import { IMetaData, MetaDataUtils } from '../metadata.model';
import { MetaDataDetailService } from './metadata-detail.service';

@Component({
  selector: 'app-metadata-detail',
  templateUrl: './metadata-detail.component.html',
  styleUrls: ['./metadata-detail.component.scss']
})
export class MetaDataDetailComponent extends BaseComponent implements OnInit {
  item: IMetaData;
  constructor(private service: MetaDataDetailService, private route: ActivatedRoute, private router: Router) {
    super();
    this.item = MetaDataUtils.init_default_value();
    this.init_toolbar_buttons(false, false);
  }

 ngOnInit() {
    this.init_top_nav_items();
    this.route.params.subscribe(res => {
      this.item = MetaDataUtils.init_default_value();
      if (res.id) {
        this.service.get(res.id).subscribe(
          (response: BaseResponseSingle) => {
            this.item = response.item;
            this.init_top_nav_items();
          }
        );
      }
    });
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'SEO Meta Data',
      url: '/metadatas',
      icon: ''
    });

    this.top_nav_items.push({
      active: false,
      text: this.item._id ? 'Edit SEO Meta Data' : 'New SEO Meta Data', 
      url: "/metadatas/" + this.item._id,
      icon: ''
    });
  }

  on_save_click() {
    this.service.save(this.item).subscribe(
      (response: BaseResponseSingle) => {
        this.router.navigateByUrl("/metadatas");
      }
    );
  }

  on_close_click() {
        this.router.navigateByUrl("/metadatas");
  }

  on_copy_click() {
    if (this.item._id) {
      this.item._id = "";
      this.item.article_title = "Copy " + this.item.article_title;
      this.service.save(this.item).subscribe(
        (response: BaseResponseSingle) => {
          this.router.navigateByUrl("/metadatas");
        }
      );
    }
  }
}