import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { BaseResponseCollection } from '../base.response';
import { BaseComponent } from '../core/base.component';
import { MetaDataService } from './metadata.service';
import { IMetaData, MetaDataUtils } from './metadata.model'

@Component({
  selector: 'app-metadata',
  templateUrl: './metadata.component.html',
  styleUrls: ['./metadata.component.scss']
})

export class MetaDataComponent extends BaseComponent implements OnInit {
  list_metadata: IMetaData[];
  selected_metadata:IMetaData;
  displayed_columns: string[] = [ 'article_title',  'article_url',  'article_type',  'action' ]; 
  constructor(private service: MetaDataService, private router:Router) { 
    super();
  }

  ngOnInit() {
    this.init_top_nav_items();
    this.on_load();
  }

  on_load() {
    this.service.list()
      .subscribe(
        (response:BaseResponseCollection) => {
          this.list_metadata = response.items;
        }
      );
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
       active: true,
      text: 'SEO Meta Data',
      url: '/metadatas',
      icon: ''
    });
  }

  on_row_delete(metadata:IMetaData){
    this.service.delete(metadata._id)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load();
        }
      );
  }

  add_click() {
    this.router.navigateByUrl(`/metadatas/new`);
  }

  on_drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list_metadata, event.previousIndex, event.currentIndex); 
    const move_item = this.list_metadata[event.currentIndex];
    this.service.re_order_items(move_item._id, event.currentIndex)
      .subscribe(
        (response:BaseResponseCollection) => {
          this.on_load();
        }
      );
  }
}