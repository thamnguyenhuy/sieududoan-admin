import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base.service';
import { environment } from '../../environments/environment';
import { IMetaData} from './metadata.model';
declare var require: any
var format = require('string-format')

@Injectable()
export class MetaDataService extends BaseService {

  constructor(protected http:HttpClient) { 
      super(http);
  }

  public list() {
    return this.__list(environment.api.urls.metadatas.list);
  }

  public get(id:string) {
    return this.__get(format(environment.api.urls.metadatas.get, id));
  }

  public get_by_url(url:string) {
    return this.__get(format(environment.api.urls.metadatas.get_by_url, url));
  }

  public save(data:IMetaData) {
    if (data._id){
      return this.__edit(format(environment.api.urls.metadatas.edit, data._id), data);
    } else {
      return this.__add(environment.api.urls.metadatas.add, data);
    }
  }

  public delete(id:string) {
      return this.__delete(format(environment.api.urls.metadatas.delete, id));
  }

  public re_order_items(id:string, new_position: number) {
      return this.__re_order_items(format(environment.api.urls.metadatas.order, id, new_position));
  }

}