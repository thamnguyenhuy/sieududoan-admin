
export interface IMetaData {
  _id: string,
  language: string,
  article_title: string,
  description: string,
  keywords: string,
  article_url: string,
  image_url: string,
  article_type: string,
  article_detail:string,

}
export class MetaDataUtils {
  public static init_default_value(): IMetaData {
    return {
      _id: "", 
      language: "vi",
      article_title: '',
      description: '',
      keywords: '',
      article_url: '',
      image_url: '',
      article_type: '',
      article_detail: '',
    }
  }
}

