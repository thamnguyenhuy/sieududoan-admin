import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { sieududoanMaterialModule } from './material-module';
import { TopNavComponent } from './shared/top-nav/top-nav.component';
import { BaseService } from './base.service';
import { TokenInterceptor } from './core/token.interceptor';
import { FormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { ContestService, ContestComponent, ContestDetailComponent, ContestDetailService } from './contest/contest.index';
import { ContestQuestionService, ContestQuestionComponent, ContestQuestionDetailComponent, ContestQuestionDetailService, ContestQuestionUtils } from './contestquestion/contestquestion.index'
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthenticationService } from './core/authentication.service';
import { FacebookModule, FacebookService } from 'ngx-facebook';
import { LoginBottomSheetComponent } from './login-bottom-sheet/login-bottom-sheet.component';
import { QuestionAnswerComponent, QuestionAnswerDetailComponent, QuestionAnswerService, QuestionAnswerDetailService } from './questionanswer/questionanswer.index';
import { MetaDataComponent, MetaDataDetailComponent, MetaDataService, MetaDataDetailService } from './metadata/metadata.index';
import { UserContestComponent } from './usercontest/usercontest.component';
import { UserContestDetailComponent } from './usercontest/usercontest-detail/usercontest-detail.component';
import { UserContestService, UserContestDetailService } from './usercontest/usercontest.index';
import { ContestWinnerComponent } from './contestwinner/contestwinner.component';
import { ContestWinnerService, ContestWinnerDetailComponent } from './contestwinner/contestwinner.index';
import { ContestResultDetailComponent } from './contest/contest-result-detail/contest-result-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    ContestComponent,
    ContestDetailComponent,
    ContestDetailComponent,
    ContestQuestionComponent,
    ContestQuestionDetailComponent,
    LoginComponent,
    LogoutComponent,
    LoginBottomSheetComponent,
    QuestionAnswerComponent,
    QuestionAnswerDetailComponent,
    ContestResultDetailComponent,
    MetaDataComponent,
    MetaDataDetailComponent,
    UserContestComponent,
    UserContestDetailComponent,
    ContestWinnerComponent,    
    ContestWinnerDetailComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    sieududoanMaterialModule,
    MarkdownModule.forRoot(),
    FacebookModule.forRoot(),
  ],
  entryComponents: [LoginBottomSheetComponent],
  providers: [
    BaseService,    
    ContestService,
    ContestDetailService,
    ContestQuestionService,
    ContestQuestionDetailService,
    AuthenticationService,
    FacebookService,
    QuestionAnswerService,
    QuestionAnswerDetailService,
    MetaDataService,
    MetaDataDetailService,
    UserContestService,
    UserContestDetailService,
    ContestWinnerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
