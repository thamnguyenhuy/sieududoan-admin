import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../core/base.component';
import { AuthenticationService } from '../core/authentication.service';
import { ITopNavConfig } from '../shared/top-nav/top-nav.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {
  top_nav_config: ITopNavConfig;
  constructor(private service: AuthenticationService, private router: Router,
  ) {
    super();
    this.top_nav_config = {
      show_add_button: false,
      show_view_mode_buttons: false,
      show_refresh_button: false,
    }
  }

  ngOnInit() {
    // this.init_top_nav_items();
    // if (this.service.is_logged_in()) {
    //   this.router.navigate(["contests"]);
    // }
  }

  init_top_nav_items() {
    this.top_nav_items = [];
    this.top_nav_items.push({
      active: true,
      text: 'Đăng nhập hệ thống',
      url: '/login',
      icon: 'lock_open',
    });
  }

  fb_login() {
    this.service.fb_login("contests", "login");
  }
}
